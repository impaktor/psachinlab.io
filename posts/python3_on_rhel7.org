#+title: Python 3.x on RHEL7
#+date: <2016-10-09>
#+filetags: python python3 rhel
#+setupfile: ../org-templates/post.org

Manually install Python-3.5 in Red Hat Enterprise Linux

**** Install required dependencies
     #+BEGIN_SRC sh -n
       yum install zlib-devel openssl-devel readline \
	   readline-devel sqlite-devel tkinter ncurses-static \
	   ncurses-base ncurses-term -y
     #+END_SRC

**** Download, Configure, & Install Python

     Download and extract the tarball
     #+BEGIN_SRC sh -n
       wget --progress=bar https://www.python.org/ftp/python/3.5.2/Python-3.5.2.tar.xz

       # Extract tarball
       tar xvJf Python-3.5.2.tar.xz
     #+END_SRC

     Configure and install
     #+BEGIN_SRC sh -n
       cd Python-3.5.2
       ./configure --prefix=/usr/local

       # make [-jX], where X is number of jobs.
       # This can be less-than or equal to number
       # of cpu-cores
       make -j4
       make install
     #+END_SRC

     /Sample tailed output/

     #+BEGIN_SRC sh -n
       Collecting setuptools
       Collecting pip
       Installing collected packages: setuptools, pip
       Found existing installation: setuptools 18.2
       Uninstalling setuptools-18.2:
	 Successfully uninstalled setuptools-18.2
       Found existing installation: pip 7.1.2
       Uninstalling pip-7.1.2:
	 Successfully uninstalled pip-7.1.2
       Successfully installed pip-8.1.1 setuptools-20.10.1
     #+END_SRC

     You should have successfully installed Python-3.5.2 by now.
     #+BEGIN_SRC sh -n
       which python3.5
       #  Output
       /usr/local/bin/python3.5
     #+END_SRC

**** [Optional] Create virtual environment

     You can go head and try creating Python virtual environment using
     [[https://docs.python.org/3/library/venv.html][pyenv]].

     #+BEGIN_SRC sh -n
       pyvenv py3
       source py3/bin/activate
     #+END_SRC

     Once inside the virtual environment, verify Python version
     #+BEGIN_SRC sh -n
       (py3) [jedi@pywar ~]$ python --version
       Python 3.5.2
     #+END_SRC

#+INCLUDE: "../disquss.inc"

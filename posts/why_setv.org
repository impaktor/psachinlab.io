#+title: Why setV?
#+date: <2020-01-19>
#+filetags: python setv
#+setupfile: ../org-templates/post.org

#+ATTR_HTML: :class center no-border
https://gitlab.com/psachin/setV/raw/master/img/logo.png

I got a [[https://opensource.com/comment/190861#comment-190861][comment]] on my [[https://opensource.com/article/20/1/setv-bash-function][post]] that what are the benefits of =setv= compared to
[[https://github.com/pyenv/pyenv][pyenv]] or auto-pyenv? Let me answer this in this post. Well, as you may have
studied the script, =setv= is actually a wrapper around Python's =venv= module
and =virtualenv= (in case of Python2). It uses =venv= or =virtualenv= depending
upon the Python version to create the Python virtual environment. At the time of
developing this script/project my goal was not only to create the Python virtual
environment but how to manage existing Python virtual environments more
efficiently. I'll share some benefits of =setv= below:

1. *Where is my virtual environment?*

   I often see this question asked by newbies in Python or Python virtual
   environment. They all love the idea of isolating the packages per project but
   they often forget to activate the virtual environment the very next time.
   Mostly they don't remember where the virtual environment is saved. This
   create confusion. The preferred way is to save the environment within the
   project itself so that you don't have spend time searching the right
   environment. This is great but it ends up creating directories like =bin/=,
   =include/=, =lib/= and =share/=. I'm not saying this is bad. Of course with
   experience these directories will not have much concerns, but this can be
   more confusing for the first timer. I've see many guys unknowingly adding
   these directories to the =git= repositories.

   Before =setv=, I used to keep all my virtual environments separately in
   =~/virtualenv= isolated from all the Python projects and then simply type:

   #+BEGIN_SRC shell
     source ~/virtualenvs/<VIRTUALENV_NAME>/bin/activate
   #+END_SRC

   to activate the specific virtual environment. This was great. Even now =setv=
   keeps all the virtual environments in =~/virtualenvs/=. This is purely
   historical. One can always change the directory name and path. But what I
   personally love about =setv= is I don't have to remember the path of any
   virtual environment. =setv= will list all the virtual environment names using
   TAB completion. No matter in which directory I'm currently in my system, I
   can always use =setv= to activate the desired virtual environment. See the
   [[https://asciinema.org/a/234059][screen-cast]] for demo. This saves a lot of time.

2. *Switching between the virtual environments.*

   This brings us to another advantage of using =setv=. We often want to switch
   between the Python projects. Naturally we also have to switch the virtual
   environment. =setv= makes this really simple as all the virtual environments
   are always available to =setv=. Try TAB completing the environment name by
   initially typing a few letters(or just one letter) of your virtual
   environment.

3. *Sharing a common virtual environment between projects.*

   We often want to use a common virtual environment between projects especially
   when the "core" and "plugins" are two separate projects. It will be tedious
   to save the virtual environment in "core" or the "plugins" and then use

   #+BEGIN_SRC shell
     source <VIRTUALENV_NAME>/bin/activate
   #+END_SRC

   to activate the project. Generally "plugins" will always have more modules on
   top of the "core", so it makes perfect sense to use a common environment for
   working on both projects.

4. *Re-using the virtual environment.*

   I personally use this a lot. I Often create a temporary virtual environment
   to try out a new project hosted on GitHub/GitLab and find it useful to re-use
   to the environment by installing more modules for a new project. This saves
   time and avoids data being downloaded twice.

5. *What about the stale virtual environments?*

   We will eventually end up having all the virtual environments in
   =~/virtualenvs=. With time, few will be unused. This will consume unnecessary
   disk space. =setv= has a =--delete= flag to clean up the old virtual
   environments and make space for a new one.

#+INCLUDE: "../disquss.inc"

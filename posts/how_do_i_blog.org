#+title: opensource.com: How do I blog?
#+date: <2020-03-05>
#+filetags: blog orgmode org-publish emacs
#+setupfile: ../org-templates/post.org

Read how do I use =org-publish= to write posts in Org mode and publish them
entirely using GNU Emacs on [[https://opensource.com/article/20/3/blog-emacs][opensource.com]].

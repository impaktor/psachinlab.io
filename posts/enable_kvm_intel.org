#+title: Enable Kernel virtualization on Intel/AMD arch
#+date: <2012-08-23>
#+filetags: linux kernel intel amd arch virtualization
#+setupfile: ../org-templates/post.org

Enable *Kernel-based Virtual Machine* on your system.

To check if the hardware supports Virtualizaton Technology(VT), open
the terminal and type
#+BEGIN_SRC bash
  egrep '(vmx|svm)' /proc/cpuinfo
#+END_SRC

/where:/

*vmx* (Virtual Machine eXtension) - denotes Intel processor

*svm* (Secure Virtual Machine) - denotes AMD processor


/If you can see a output that means your hardware supports VT./


Next step is to load the kernel modules(Irrespective of your processor type)
#+BEGIN_SRC bash -n
  sudo modprobe kvm_intel
  sudo modprobe kvm
#+END_SRC

#+INCLUDE: "../disquss.inc"
